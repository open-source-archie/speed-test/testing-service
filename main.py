import logging

from dotenv import load_dotenv

import speed_test
from repo import PostgresRepo
from service import MainService


def main():
    # Setup logging
    logging.basicConfig(
        level=logging.WARNING, format="%(asctime)s:%(name)s:%(levelname)s:%(message)s"
    )

    # Load config
    load_dotenv()

    # Construct speed tester
    speed_test_service = speed_test.SpeedTestService()

    # Construct repo
    repo = PostgresRepo()

    # Construct service
    main_service = MainService(speed_test_service, repo)

    main_service.run_service()


if __name__ == "__main__":
    main()
