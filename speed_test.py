import logging
import time

import speedtest
from retry import retry

# Configure new logger
logger = logging.getLogger(__name__)


class BestServerError(Exception):
    pass


class SpeedTestService:
    def __init__(self):
        self.speed_test_obj = speedtest.Speedtest()

    @retry(delay=1, backoff=2, tries=7)
    def _measure_speed(self):
        """Identifies the best server based on latency then completes the metrics testing."""

        s = self.speed_test_obj

        try:
            # Identify best server based on latency
            best_server = s.get_best_server()

            # Attempts to find another server if the latency is too high
            for i in range(5):
                if best_server["latency"] >= 130:
                    logger.warning(
                        f"Latency to best server too high at {best_server['latency']}, attempt {i+1}. Retrying..."
                    )
                    best_server = s.get_best_server()
                    time.sleep(2)
                else:
                    break

            logger.debug(f'Server identified at {best_server["host"]}')

        except BestServerError as err:
            logger.exception(err)

        # Test
        s.download()
        s.upload()
        results = s.results.dict()
        logger.debug("Test completed")

        return results

    def filter_test_results(self):
        """Returns the filtered results from the internet metrics test"""
        relevant_keys = [
            "timestamp",
            "download",
            "bytes_received",
            "upload",
            "bytes_sent",
            "ping",
            "server:url",
            "server:name",
            "server:country",
            "server:sponsor",
            "server:host",
            "client:isp",
            "client:isprating",
        ]
        results = self._measure_speed()

        formatted_dict = {}
        for key in relevant_keys:
            if ":" not in key:
                # Returns any key that does not contain ':'
                formatted_dict[key] = results[key]
            else:
                # Returns any nested keys as first order keys
                key1, key2 = key.split(":")
                formatted_dict[f"{key1}_{key2}"] = results[key1][key2]

        return formatted_dict
