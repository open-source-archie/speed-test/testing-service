import logging
import os

import sqlalchemy.schema
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Base

# Configure new logger
logger = logging.getLogger(__name__)


class PostgresRepo:
    def __init__(self):
        self.db = os.environ["DB_NAME"]
        self.user = os.environ["DB_USER"]
        self.password = os.environ["DB_PASSWORD"]
        self.host = os.environ["DB_HOST"]
        self.port = os.environ["DB_PORT"]

    def get_engine(self):
        """Creates an :class:`Engine` instance to interface with the database"""
        return create_engine(
            f"postgresql://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"
        )

    def create_schema(self):
        """Creates a new schema called 'internet_metrics' unless one already exists"""
        engine = self.get_engine()
        if not engine.dialect.has_schema(engine, "internet_metrics"):
            engine.execute(sqlalchemy.schema.CreateSchema("internet_metrics"))

    def get_session(self):
        session = sessionmaker()
        session.configure(bind=self.get_engine())
        return session()

    def build_table(self):
        Base.metadata.create_all(self.get_engine())

    def insert_to_db(self, obj):
        session = self.get_session()
        session.add(obj)
        session.commit()
        logger.debug("Data added to the database")
