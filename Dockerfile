FROM python:3.10.0-alpine

LABEL org.opencontainers.image.authors="https://gitlab.com/archieskeoch"

WORKDIR /speed-tester

CMD ["crond", "-f"]

RUN apk add --no-cache --virtual .build-deps python3-dev gcc musl-dev postgresql-dev build-base

COPY requirements.txt /speed-tester

RUN pip install --no-cache -r requirements.txt
RUN apk del python3-dev gcc musl-dev build-base

RUN echo "*/10 * * * * python /speed-tester/main.py" | crontab -

COPY . /speed-tester
