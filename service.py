import models


class MainService:
    def __init__(self, speed_test_service, repo):
        self.speed_test = speed_test_service
        self.pg_repo = repo

    def run_service(self):
        # Collect the test data
        test_results = self.speed_test.filter_test_results()

        # Create schema if none
        self.pg_repo.create_schema()

        # Create ORM model
        obj = models.SpeedResult(**test_results)

        # Build table
        self.pg_repo.build_table()

        # Insert model on database
        self.pg_repo.insert_to_db(obj)
